﻿using log4net;
using System.Collections;

namespace LogServiceHelper
{
    public sealed class LogHelper
    {
        static volatile ILog _log;
        private static readonly Hashtable Ht = new Hashtable();

        public static ILog Logger
        {
            get
            {
               if(_log == null)
               {
                   lock (Ht.SyncRoot)
                   {
                       if (_log == null)
                       {
                           _log = LogManager.GetLogger(System.Configuration.ConfigurationManager.AppSettings["LogKey"] ?? "WindowsServiceLog");
                       }
                   }
               }

                return _log;
            }
        }
        
        //public static void WriteMessage(string message)
        //{
        //    File.AppendAllText("D:\\logservice.txt", message);
        //}

       
    }
}
