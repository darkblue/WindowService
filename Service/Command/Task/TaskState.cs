namespace WindowServices.Service.Command.Task
{
    public enum TaskState
    {
        None,
        Success,
        Error,
    }
}