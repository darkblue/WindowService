using System;

namespace WindowServices.Service.Command
{
    internal sealed class InvokeCommand
    {
        private ICommand _command;
        public void SetCommand(ICommand command)
        {
            _command = command;
        }

        public void Execute()
        {
            try
            {
                _command.Execute();
            }
            catch (Exception)
            {
                _command.RollBack();
                throw;
            }
        }
    }
}