using System.Collections.Generic;
using WindowServices.Service.Command.Task;

namespace WindowServices.Service.Command
{
    internal abstract class CommandBase : ICommand
    {
        private readonly List<ITask> _list;
        protected CommandBase(List<ITask> list)
        {
            _list = list;
            _list.Sort((c1, c2) => c1.Order.CompareTo(c2.Order));
        }
        public void Execute()
        {
            _list.ForEach(c => c.Execute());
        }

        public void RollBack()
        {
            var tasks = new List<ITask>();
            tasks.AddRange(_list);
            tasks.Reverse();
            tasks.ForEach(c => c.RollBack());
        }
    }
}