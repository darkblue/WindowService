﻿using System.Collections.Generic;
using WindowServices.Common;
using WindowServices.Service.Command.Task;
using System.Linq;
using WindowServices.Service.Properties;

namespace WindowServices.Service.Command
{
    internal sealed class UnLoadServiceCommand : ICommand
    {
        private readonly Dictionary<string, ServiceHelper> _services;
        public UnLoadServiceCommand(Dictionary<string, ServiceHelper> services)
        {
            _services = services;
        }

        public void Execute()
        {
            var list = _services.Keys.ToList();
            LogHelper.WriteEntityServiceInfo(Resources.StopServerStart, EventType.Info);

            foreach (var key in list)
            {
                var sh = _services[key];
                _services.Remove(key);
                Utility.UNload(key, sh);
            }

            _services.Clear();
            LogHelper.WriteEntityServiceInfo(Resources.StopServerSuccess, EventType.Info);
            System.Environment.Exit(0);
        }

        public void RollBack()
        {

        }
    }
}